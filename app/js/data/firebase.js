import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyC1KSCFsp2KdAUKNXcgkpAy-qgbEpqfgB0",
  authDomain: "goods-sait-8c884.firebaseapp.com",
  databaseURL: "https://goods-sait-8c884-default-rtdb.firebaseio.com",
  projectId: "goods-sait-8c884",
  storageBucket: "goods-sait-8c884.appspot.com",
  messagingSenderId: "238858711928",
  appId: "1:238858711928:web:6faee673d0b501063b8e8d",
};

firebase.initializeApp(firebaseConfig);

export const database = firebase.database();
export const auth = firebase.auth();
