import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { validGoods } from "../../utils/validUtils/validGoods.js";
import { filterCategory } from "../../utils/arrayUtils/filterCategory.js";
import { filterSize } from "../../utils/arrayUtils/filterSize.js";
const shoesWrapper = document.querySelector("#shoes-wrapper");
const shoesDots = document.querySelector("#shoes-dots");
const shoesCategory = document.querySelectorAll("#shoes-list li");
const shoesSize = document.querySelectorAll("#shoes-size button");
const shoesColor = document.querySelectorAll("#shoes-color .category__box");
const shoesPrice = document.querySelectorAll("#shoes-price input");
const shoesForm = document.querySelector("#shoes-form");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const shoes = goodsFilter(goods)("type", "shoes");
  // filter category
  shoesCategory.forEach((item) => {
    item.addEventListener("click", () => {
      shoesCategory.forEach((el) => el.classList.remove("active"));
      const result = filterCategory(shoes)(item, "category");
      validGoods(result)(shoesWrapper, shoesDots);
    });
  });
  // filter size
  shoesSize.forEach((item) => {
    item.addEventListener("click", () => {
      shoesSize.forEach((el) => el.classList.remove("active"));
      const result = filterSize(shoes, item);
      validGoods(result)(shoesWrapper, shoesDots);
    });
  });
  // filter color
  shoesColor.forEach((item) => {
    item.addEventListener("click", () => {
      const content = item.querySelector("p");
      shoesColor.forEach((el) => el.classList.remove("active"));
      const result = filterCategory(shoes)(content, "color");
      validGoods(result)(shoesWrapper, shoesDots);
    });
  });
  // filter price
  shoesPrice.forEach((item) => {
    item.addEventListener("change", () => {
      shoesPrice.forEach((el) => (el.checked = false));
      item.checked = true;
      const fromPrice = +item.dataset.from;
      const beforePrice = +item.dataset.before;
      const result = shoes.filter(
        (item) => item.price >= fromPrice && item.price <= beforePrice
      );
      validGoods(result)(shoesWrapper, shoesDots);
    });
  });
  // filter form
  shoesForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const fromPrice = +e.target.from.value;
    const beforePrice = +e.target.before.value;
    const result = shoes.filter(
      (item) => item.price >= fromPrice && item.price <= beforePrice
    );
    validGoods(result)(shoesWrapper, shoesDots);
  });
});

shoesForm.from.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});
shoesForm.before.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});