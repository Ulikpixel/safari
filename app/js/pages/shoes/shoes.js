import { goodsRender } from "../../utils/renderUtils/renderGood.js";
import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { componentsRender } from '../../utils/renderUtils/componentsRender.js';
import { addDataFavorites } from '../../state/categoryState/addDataFavorites.js';
const shoesWrapper = document.querySelector("#shoes-wrapper");
const shoesDots = document.querySelector("#shoes-dots");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const showPage = 6;
  const shoes = goodsFilter(goods)("type", "shoes");
  const [shoesLayout, dotsLayout] = goodsRender(shoes)(1, showPage);
  componentsRender([shoesWrapper, shoesDots])([shoesLayout, dotsLayout]);
  addDataFavorites(shoes);
});
