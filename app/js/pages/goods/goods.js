import { goodsRender } from "../../utils/renderUtils/renderGood.js";
import { database } from "../../data/firebase.js";
import { componentsRender } from "../../utils/renderUtils/componentsRender.js";
import { addDataFavorites } from '../../state/categoryState/addDataFavorites.js';
const goodsWrapper = document.querySelector("#goods-wrapper");
const goodsDots = document.querySelector("#goods-dots");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const showPage = 16;
  const [goodsLayout, dotsLayout] = goodsRender(goods)(1, showPage);
  componentsRender([goodsWrapper, goodsDots])([goodsLayout, dotsLayout]);
  addDataFavorites(goods);
});
