import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { validGoods } from '../../utils/validUtils/validGoods.js';
import { filterCategory } from '../../utils/arrayUtils/filterCategory.js';
import { filterSize } from '../../utils/arrayUtils/filterSize.js';
const accessoriesWrapper = document.querySelector("#accessories-wrapper");
const accessoriesDots = document.querySelector("#accessories-dots");
const accessoriesCategory = document.querySelectorAll("#accessories-list li");
const accessoriesSize = document.querySelectorAll("#accessories-size button");
const accessoriesColor = document.querySelectorAll("#accessories-color .category__box");
const accessoriesPrice = document.querySelectorAll("#accessories-price input");
const accessoriesForm = document.querySelector("#accessories-form");
 
database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const accessories = goodsFilter(goods)("type", "accessories");
  // filter category 
  accessoriesCategory.forEach(item => {
    item.addEventListener('click', () => {
      accessoriesCategory.forEach(el => el.classList.remove('active'));
      const result = filterCategory(accessories)(item, 'category');
      validGoods(result)(accessoriesWrapper, accessoriesDots);
    })
  })
  // filter size 
  accessoriesSize.forEach(item => {
    item.addEventListener('click', () => {
      accessoriesSize.forEach(el => el.classList.remove('active'));
      const result = filterSize(accessories, item);
      validGoods(result)(accessoriesWrapper, accessoriesDots);
    })
  })
  // filter color 
  accessoriesColor.forEach(item => {
    item.addEventListener('click', () => {
      const content = item.querySelector('p');
      accessoriesColor.forEach(el => el.classList.remove('active'));
      const result = filterCategory(accessories)(content, 'color');
      validGoods(result)(accessoriesWrapper, accessoriesDots);
    })
  })
  // filter price
  accessoriesPrice.forEach(item => {
    item.addEventListener('change', () => {
      accessoriesPrice.forEach(el => el.checked = false);
      item.checked = true;
      const fromPrice = +item.dataset.from;
      const beforePrice = +item.dataset.before;
      const result = accessories.filter(item => item.price >= fromPrice && item.price <= beforePrice);
      validGoods(result)(accessoriesWrapper, accessoriesDots);
    })
  })
  // filter form
  accessoriesForm.addEventListener('submit', e => {
    e.preventDefault();
    const fromPrice = +e.target.from.value;
    const beforePrice = +e.target.before.value;
    const result = accessories.filter(item => item.price >= fromPrice && item.price <= beforePrice);
    validGoods(result)(accessoriesWrapper, accessoriesDots);
  })
});

accessoriesForm.from.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});
accessoriesForm.before.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});