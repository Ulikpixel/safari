import { goodsRender } from "../../utils/renderUtils/renderGood.js";
import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { componentsRender } from '../../utils/renderUtils/componentsRender.js';
import { addDataFavorites } from '../../state/categoryState/addDataFavorites.js';
const accessoriesWrapper = document.querySelector("#accessories-wrapper");
const accessoriesDots = document.querySelector("#accessories-dots");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const showPage = 6;
  const accessories = goodsFilter(goods)("type", "accessories");
  const [accessoriesLayout, dotsLayout] = goodsRender(accessories)(1, showPage);
  componentsRender([accessoriesWrapper, accessoriesDots])([accessoriesLayout, dotsLayout]);
  addDataFavorites(accessories);
});

  