const popup = document.querySelector('.modal__order--popup');
const close = document.querySelector('.modal__order--close');
const modalWindow = document.querySelector('.modal__order');

const closeModal = () => {
    modalWindow.classList.remove('active');
    popup.classList.remove('active');
};

close.addEventListener('click', closeModal);
popup.addEventListener('click', closeModal);