import { goodsRender } from "../../utils/renderUtils/renderGood.js";
import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { componentsRender } from "../../utils/renderUtils/componentsRender.js";
import { addDataFavorites } from '../../state/categoryState/addDataFavorites.js';
const clothesWrapper = document.querySelector("#clothes-wrapper");
const clothesDots = document.querySelector("#clothes-dots");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const showPage = 6;
  const clothes = goodsFilter(goods)("type", "clothes");
  const [clothesLayout, dotsLayout] = goodsRender(clothes)(1, showPage);
  componentsRender([clothesWrapper, clothesDots])([clothesLayout, dotsLayout]);
  addDataFavorites(clothes);
});
