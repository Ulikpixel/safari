import { database } from "../../data/firebase.js";
import { goodsFilter } from "../../utils/arrayUtils/goodsFilter.js";
import { validGoods } from '../../utils/validUtils/validGoods.js';
import { filterCategory } from '../../utils/arrayUtils/filterCategory.js';
import { filterSize } from '../../utils/arrayUtils/filterSize.js';
const clothesWrapper = document.querySelector("#clothes-wrapper");
const clothesDots = document.querySelector("#clothes-dots");
const clothesCategory = document.querySelectorAll("#clothes-category li");
const clothesSize = document.querySelectorAll("#clothes-size button");
const clothesColor = document.querySelectorAll("#clothes-color .category__box");
const clothesPrice = document.querySelectorAll("#clothes-price input");
const clothesForm = document.querySelector("#clothes-from");

database.ref("/goods/").on("value", (snpashot) => {
  const goods = snpashot.val();
  const clothes = goodsFilter(goods)("type", "clothes");
  // filter category 
  clothesCategory.forEach(item => {
    item.addEventListener('click', () => {
      clothesCategory.forEach(el => el.classList.remove('active'));
      const result = filterCategory(clothes)(item, 'category');
      validGoods(result)(clothesWrapper, clothesDots);
    })
  })
  // filter size 
  clothesSize.forEach(item => {
    item.addEventListener('click', () => {
      clothesSize.forEach(el => el.classList.remove('active'));
      const result = filterSize(clothes, item);
      validGoods(result)(clothesWrapper, clothesDots);
    })
  })
  // filter color 
  clothesColor.forEach(item => {
    item.addEventListener('click', () => {
      const content = item.querySelector('p');
      clothesColor.forEach(el => el.classList.remove('active'));
      const result = filterCategory(clothes)(content, 'color');
      validGoods(result)(clothesWrapper, clothesDots);
    })
  })
  // filter price
  clothesPrice.forEach(item => {
    item.addEventListener('change', () => {
      clothesPrice.forEach(el => el.checked = false);
      item.checked = true;
      const fromPrice = +item.dataset.from;
      const beforePrice = +item.dataset.before;
      const result = clothes.filter(item => item.price >= fromPrice && item.price <= beforePrice);
      validGoods(result)(clothesWrapper, clothesDots);
    })
  })
  // filter form
  clothesForm.addEventListener('submit', e => {
    e.preventDefault();
    const fromPrice = +e.target.from.value;
    const beforePrice = +e.target.before.value;
    const result = clothes.filter(item => item.price >= fromPrice && item.price <= beforePrice);
    validGoods(result)(clothesWrapper, clothesDots);
  })
});

clothesForm.from.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});
clothesForm.before.addEventListener('input', (e) => {
  if(isNaN(e.target.value)){
    e.target.value = e.target.value.replace(/[^\d]/g, '');
  }
});



