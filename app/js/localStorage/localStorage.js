export const getDataStorage = (name) => {
  const result = JSON.parse(localStorage.getItem(name));
  return result;
};

export const setGoodsStorage = (name, data) => {
  const goods = getDataStorage(name);
  const uniqueGoods = goods.some(item => item.id === data[0].id);
  if (!uniqueGoods) {
    const newGoods = [...goods, ...data];
    const result = JSON.stringify(newGoods);
    localStorage.setItem(name, result);
  }
};


export const removeGoodsStorage = (name, id) => {
  const goods = getDataStorage(name);
  const newGoods = goods.filter(item => item.id !== id);
  localStorage.setItem(name, JSON.stringify(newGoods));
}

// valid 

if (!getDataStorage('goods')) {
  localStorage.setItem('goods', '[]')
}
if (!getDataStorage('favorites')) {
  localStorage.setItem('favorites', '[]')
}




