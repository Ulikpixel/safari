import { createTag } from "../utils/createUtils/createTag.js";

export const WARNING = createTag('h1', 'category__warning');
WARNING.innerText = 'Sorry there is no such item!';