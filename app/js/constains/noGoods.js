import { createTag } from "../utils/createUtils/createTag.js";
export const NOGOODS = createTag('h1', 'warning');
NOGOODS.innerText = 'There are no items in your cart!';
