import { authLayout } from './auth/authLayout.js';
import { auth, database } from '../../data/firebase.js';
import { getUserLayout } from './user/userLayout.js';
import { sidebar } from '../../state/accountState/sidebar.js';
import { authValid } from '../../state/accountState/authValid.js';
import { changesAccount } from '../../state/accountState/changesAccount.js';
import { logout } from '../../state/accountState/logout.js';
import { getDataStorage } from '../../localStorage/localStorage.js';
import { getFavorites } from '../../state/accountState/getFavorites.js';
import { removeFavorites } from '../../state/accountState/removeFavorites.js';
import { renderPageBasket } from '../basket/basketPages.js';
const adminPanel = document.querySelectorAll('.admin-panel');
const account = document.querySelector('#account');

auth.onAuthStateChanged(user => {
	if (user) {
		const id = user.uid;
		database.ref(`users/${id}/`).on('value', (snpashot) => {
			try {
				// user data
				const userData = snpashot.val();
				// layout
				const favoritesGoods = getDataStorage('favorites');
				const userLayout = getUserLayout(userData, favoritesGoods);
				while (account.firstChild) {
					account.removeChild(account.firstChild);
				}
				account.append(userLayout);
				// state pages
				sidebar();
				logout();
				changesAccount(id, userData.role);
				getFavorites();
				removeFavorites();
				if (userData.role === 'admin') {
					adminPanel.forEach(el => el.classList.add('active'));
				} else {
					adminPanel.forEach(el => el.classList.remove('active'));
				};
			} catch (e) {
				console.log(e)
			}
		})
	} else {
		while (account.firstChild) {
			account.removeChild(account.firstChild);
		}
		account.append(authLayout());
		authValid();
		adminPanel.forEach(el => el.classList.remove('active'));
		renderPageBasket();
	}
})

