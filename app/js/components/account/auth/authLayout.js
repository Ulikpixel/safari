import { createTag } from "../../../utils/createUtils/createTag.js";

export const authLayout = () => {
  const layout = createTag("div", "container");
  layout.innerHTML = `<h3 class="account__title">Hello there!</h3>
    <p class="account__continue">Please sign in or create account to continue</p>
    <div class="account__wrapper">
      <div class="account__sign">
        <p class="account__subtitle">SIGN IN</p>
        <form class="account__sign--form">
          <p class="account__text">Email</p>
          <input type="email" name="email" />
          <p class="account__text">Password</p>
          <input type="password" name="password" />
          <div class="account__condition">
            <input type="checkbox" name="checkbox" id="sign-in" />
            <label for="sign-in" class="account__text">Remeber my details</label>
          </div>
          <button class="account__sign--btn" type="submit">SIGN IN</button>
          <a href="#" class="account__sign--ask">Forgot password?</a>
        </form>
      </div>
      <div class="account__create">
        <p class="account__subtitle">CREATE ACCOUNT</p>
        <form class="account__create--form">
          <p class="account__text">First name</p>
          <input type="text" name="name">
          <p class="account__text">Last name</p>
          <input type="text" name="lastname">
          <p class="account__text">Email</p>
          <input type="email" name="email">
          <p class="account__text">Create Password</p>
          <input type="password" name="create_password">
          <p class="account__text">Confirm Password</p>
          <input type="password" name="confirm_password">
          <div class="account__condition">
              <input type="checkbox" name="checkbox" id="create-account">
              <label for="create-account" class="account__text">I want to receive Safari newsletters with the best deals and offers</label>
          </div>
          <button class="account__create--btn" type="submit">CREATE ACCOUNT</button>
        </form>
      </div>
    </div>`;
  return layout;
}
