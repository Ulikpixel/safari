export const favoritesCarts = (favoritesGoods) => {
  const carts = favoritesGoods.map(({ id, title, size, price, img }) => {
    const cart =  document.createElement('div');
    cart.className = 'account__favorites--cart';
    cart.innerHTML = `
        <div class="account__favorites--description">
          <img src="${img}" alt="goods">
          <div class="account__favorites--content">
            <p class="account__favorites--title">${title}</p>
            <p class="account__favorites--size">Size - ${size}</p>
            <p class="account__favorites--price">₦${price}</p>
          </div>
        </div>
        <div class="account__favorites--btns">
          <button class="account__favorites--buy">
            BUY NOW
          </button>
          <button class="account__favorites--remove" data-id="${id}">
            <svg width="16" height="16" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M7.999 0A8 8 0 000 8a8 8 0 107.999-8zm3.44 10.166l-1.275 1.273S8.148 9.276 7.998 9.276c-.148 0-2.164 2.163-2.164 2.163l-1.275-1.273S6.724 8.18 6.724 8.003c0-.18-2.165-2.167-2.165-2.167l1.275-1.275s2.032 2.164 2.164 2.164c.133 0 2.166-2.164 2.166-2.164l1.274 1.275S9.273 7.852 9.273 8.003c0 .144 2.165 2.163 2.165 2.163z" />
              </svg>
            REMOVE
          </button>
        </div>
    `
    return cart;
  });
  return carts;
};