import { createTag } from '../../../utils/createUtils/createTag.js';
import { favoritesCarts } from './favoritesCarts.js';
import { NOGOODS } from '../../../constains/noGoods.js';

export const getUserLayout = (userInfo, favoritesGoods) => {
  const { name, lastname, email, data } = userInfo;
  const userLayout = createTag('div', 'container');
  const carts = favoritesCarts(favoritesGoods);
  userLayout.innerHTML = `
        <div class="account__user">
          <aside class="account__sidebar">
            <p class="account__sidebar--title">ACCOUNT DASHBOARD</p>
            <ul class="account__sidebar--list">
              <li class="account__sidebar--item active">
                <svg width="14" height="17" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M6.894 8.083A3.91 3.91 0 009.752 6.9a3.91 3.91 0 001.184-2.858 3.91 3.91 0 00-1.184-2.857A3.912 3.912 0 006.894 0a3.91 3.91 0 00-2.857 1.184 3.91 3.91 0 00-1.184 2.857A3.91 3.91 0 004.037 6.9a3.912 3.912 0 002.857 1.184zm7.072 4.82a9.99 9.99 0 00-.136-1.06 8.352 8.352 0 00-.26-1.066 5.262 5.262 0 00-.439-.994 3.751 3.751 0 00-.66-.861c-.262-.25-.58-.45-.95-.597a3.28 3.28 0 00-1.212-.22c-.171 0-.337.071-.657.28a88.68 88.68 0 01-.684.44 3.92 3.92 0 01-.886.39A3.516 3.516 0 017 9.392a3.52 3.52 0 01-1.083-.175 3.911 3.911 0 01-.885-.39 93.785 93.785 0 01-.685-.442c-.32-.208-.486-.278-.657-.278-.436 0-.844.073-1.212.22a2.911 2.911 0 00-.949.596c-.25.238-.472.528-.66.86-.183.321-.33.656-.439.995a8.377 8.377 0 00-.26 1.066 9.951 9.951 0 00-.136 1.06c-.023.322-.034.655-.034.992 0 .876.278 1.585.828 2.108.542.516 1.26.778 2.132.778h8.08c.873 0 1.59-.262 2.132-.778.55-.523.828-1.232.828-2.108 0-.339-.011-.672-.034-.992z" />
                </svg>
                Account Information
              </li>
              <li class="account__sidebar--item">
                <svg width="14" height="17" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M1.004 5.054H.471a.47.47 0 000 .942h.533v-.942zm0 7.533H.471a.47.47 0 000 .942h.533v-.942zm0-2.511H.471a.47.47 0 000 .942h.533v-.942zm0-2.511H.471a.47.47 0 000 .942h.533v-.942zm0-5.022H.471a.47.47 0 000 .941h.533v-.941zM4.018 0h-1.6c-.78 0-1.413.634-1.413 1.413v1.13H2.48a.47.47 0 010 .941H1.005v1.57H2.48a.47.47 0 010 .942H1.005v1.569H2.48a.47.47 0 010 .942H1.005v1.57H2.48a.47.47 0 010 .94H1.005v1.57H2.48a.47.47 0 010 .942H1.005v1.13c0 .78.634 1.413 1.412 1.413h1.601V0zm3.923 3.484h2.135v1.068H7.94V3.484z"
                    fill="#000" />
                  <path
                    d="M12.588 0H4.96v16.072h7.628c.779 0 1.412-.634 1.412-1.413V1.413C14 .633 13.367 0 12.588 0zm-1.57 5.022a.47.47 0 01-.47.471H7.47a.47.47 0 01-.47-.47v-2.01c0-.26.21-.47.47-.47h3.076c.26 0 .471.21.471.47v2.01z" />
                </svg>
                Address Book
              </li>
              <li class="account__sidebar--item">
                <svg width="14" height="14" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M13.087 2.715h-1.02c.135-.213.224-.452.25-.704a1.796 1.796 0 00-1.07-1.852 1.79 1.79 0 00-1.95.314L8.01 1.639a1.366 1.366 0 00-2.023.002L4.7.473A1.79 1.79 0 002.748.159a1.796 1.796 0 00-1.067 1.852c.025.252.114.491.248.704H.913A.913.913 0 000 3.628v1.37c0 .251.204.456.457.456h13.086A.457.457 0 0014 4.997v-1.37a.913.913 0 00-.913-.912zM5.63 2.562v.153H3.483a.9.9 0 01-.873-1.12A.896.896 0 013.104 1a.879.879 0 01.983.149L5.63 2.548v.013zm5.78-.673c-.034.475-.462.826-.938.826H8.37v-.169L9.89 1.17c.233-.212.56-.311.863-.221.434.13.69.499.657.941zM.913 6.367v6.695c0 .505.409.914.913.914h4.261v-7.61H.913zm7 0v7.609h4.26a.913.913 0 00.914-.914V6.367H7.913z" />
                </svg>
                My Orders
              </li>
              <li class="account__sidebar--item" id="favorites-btn">
                <svg width="14" height="13" xmlns="http://www.w3.org/2000/svg">
                  <path
                    d="M12.89 1.219A3.765 3.765 0 0010.09 0c-.809 0-1.55.255-2.2.76a4.5 4.5 0 00-.89.928 4.498 4.498 0 00-.89-.929A3.521 3.521 0 003.91 0a3.765 3.765 0 00-2.8 1.219C.395 1.996 0 3.057 0 4.207c0 1.184.441 2.268 1.389 3.411.847 1.023 2.065 2.06 3.475 3.263.482.41 1.028.876 1.595 1.371a.82.82 0 001.082 0c.567-.495 1.113-.96 1.595-1.372 1.41-1.201 2.628-2.24 3.476-3.262C13.559 6.475 14 5.391 14 4.208c0-1.151-.395-2.212-1.11-2.99z" />
                </svg>
                My Favorites
              </li>
            </ul>
            <button class="account__sidebar--out">
              <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M11.207 3.48V.782A.784.784 0 0010.426 0H.782A.784.784 0 000 .782v11.051c0 .287.17.56.417.69L6.36 15.6c.26.13.573-.052.573-.351V12.25h3.493c.43 0 .781-.352.781-.782V7.194H9.644v3.101c0 .222-.17.391-.391.391H6.946V4.053c0-.287-.17-.56-.417-.69l-3.506-1.8h6.23c.221 0 .39.17.39.39v1.539h1.564V3.48z"
                  fill="#ED165F" />
                <path
                  d="M13.215 2.202l2.541 2.542a.78.78 0 010 1.133L13.215 8.42c-.326.325-.821.338-1.147.013-.313-.313-.274-.834.026-1.147l1.147-1.134H8.797a.771.771 0 01-.586-.26c-.365-.392-.274-1.082.195-1.343a.835.835 0 01.391-.104h4.444S12.107 3.31 12.094 3.31c-.3-.3-.339-.834-.026-1.134.313-.312.821-.3 1.147.026z"
                  fill="#ED165F" /></svg>
              SIGN OUT
            </button>
          </aside>
          <div class="account__list">
            <div class="account__information account__boxes active">
              <p class="account__title">
                User Information
              </p>
              <div class="account__information--wrapper">
                <form class="account__information--information">
                  <p class="account__information--text">First name</p>
                  <input type="text" name="name" value="${name}">
                  <p class="account__information--text">Last name</p>
                  <input type="text" name="lastname" value="${lastname}">
                  <p class="account__information--text">Email</p>
                  <input type="email" name="email" value="${email}" disabled>
                  <p class="account__information--text">Gender</p>
                  <select name="gender">
                    <option value="man">man</option>
                    <option value="woman">woman</option>
                  </select>
                  <p class="account__information--text">Date of birth</p>
                  <input type="text" name="data" value="${data}">
                  <div class="account__information--condition">
                    <input type="checkbox" name="checkbox">
                    <label>Newsletter subsciption</label>
                  </div>
                </form>
              </div>
              <button class="account__information--btn">save changes</button>
            </div>
            <div class="account__book account__boxes">
              <p class="account__title">
                Address Book
              </p>
              <div class="account__book--wrapper">
                <p class="account__book--title">Default Shipping Address</p>
                <ul class="account__book--list">
                  <li class="account__book--item">Ayokunle Oriolowo</li>
                  <li class="account__book--item">No 4 Barnawa Close, Barnawa, Kaduna</li>
                  <li class="account__book--item">Kaduna, Kaduna</li>
                  <li class="account__book--item">Nigeria</li>
                  <li class="account__book--item">+234 708 568 5878</li>
                </ul>
                <div class="account__book--btns">
                  <button class="account__book--edit">Edit</button>
                  <button class="account__book--delete">Delete</button>
                </div>
              </div>
            </div>
            <div class="account__orders account__boxes">
              <p class="account__title">
                My Orders
              </p>
              <div class="account__orders--wrapper">
                <div class="account__orders--cart">
                  <div class="account__orders--description">
                    <div class="account__description--show">
                      <img src="./images/dist/answer.png" alt="image">
                      <p class="account__description--status">delivered</p>
                    </div>
                    <div class="account__description--content">
                      <p class="account__description--subtitle">Casual flat loafers</p>
                      <p class="account__description--size">Size - EU: 36 US: 5.5</p>
                      <p class="account__description--price">₦ 10,000</p>
                      <p class="account__description--data">On 20/07/2020</p>
                    </div>
                  </div>
                  <div class="account__orders--price">
                    <p class="account__price--title">Payment details</p>
                    <p class="account__price--items">Item’s total - ₦ 10,000 </p>
                    <p class="account__price--delivery">Delivery fee - ₦ 2,000 </p>
                    <p class="account__price--total">TOTAL - ₦ 12,000 </p>
                  </div>
                  <div class="account__orders--details">
                    <div class="account__details--method">
                      <p class="account__details--title">Delivery method</p>
                      <p class="account__details--text">Door delivery</p>
                    </div>
                    <div class="account__details--address">
                      <p class="account__details--title">Shipping address</p>
                      <p class="account__details--text">Ayokunle Oriolowo</p>
                      <p class="account__details--text">4, Barnawa Close, Barnawa Kaduna.</p>
                    </div>
                  </div>
                </div>
                <div class="account__orders--btns">
                  <button class="account__orders--again">ORDER AGAIN</button>
                  <button class="account__orders--return">REQUEST A RETURN</button>
                </div>
              </div>
            </div>
            <div class="account__favorites account__boxes">
              <p class="account__title">
                My Orders
              </p>
              <div class="account__favorites--wrapper">
                ${ favoritesGoods.length ? carts : '<h1 class="warning">There are no items in your cart!</h1>' }
              </div>
            </div>
          </div>
        </div>
  `
  return userLayout;
}
