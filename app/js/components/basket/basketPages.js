import { basketLayout } from './basketLayout/basketLayout.js';
import { removeGoods } from '../../state/basketState/removeGoods.js';
import { addDataBasket } from '../../state/basketState/addDataBasket.js';
import { checkoutLayout } from './checkoutLayout/checkoutLayout.js';
import { openPageCheckout } from '../../state/basketState/openPageCheckout.js';
import { openShop } from '../../state/basketState/openShop.js';
import { openPageFavorites } from '../../state/basketState/openPageFavorites.js';
const basketPage = document.querySelector('#basket .container');

export const renderPageBasket = () => {
    while (basketPage.firstChild) {
        basketPage.removeChild(basketPage.firstChild);
    }
    basketPage.append(basketLayout());
    removeGoods(renderPageBasket); // a function that removes items from the cart
    addDataBasket(renderPageBasket); // function that adds items to the cart
    openPageCheckout(basketPage, checkoutLayout); // function opens the page checkout
    openShop();
    openPageFavorites(basketPage);
}

renderPageBasket();



