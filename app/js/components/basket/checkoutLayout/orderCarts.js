export const orderCarts = (goods) => {
    const counter = document.querySelectorAll('.basket__checkout--count');
    const carts = `
    ${goods.map(({ img, title, size, price}, i) => {
        const quantity = counter[i].innerText;
        const total = price * +counter[i].innerText;
        return `
            <div class="modal__order--cart">
                <img src="${img}" alt="image" />
                <div class="modal__order--description">
                    <p class="modal__order--title">${title}</p>
                    <p class="modal__order--size"> Size - ${size}</p>
                    <p class="modal__order--quantity">Quantity - ${quantity}</p>
                    <p class="modal__order--price"> Price - ${total}</p>
                </div>
            </div>
        `
        }).join('')}
    `
    return carts;
}