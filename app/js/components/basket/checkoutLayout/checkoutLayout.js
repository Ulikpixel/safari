import { createTag } from "../../../utils/createUtils/createTag.js";
import { getDataStorage } from '../../../localStorage/localStorage.js';
import { checkoutCarts } from './checkoutCarts.js';

export const checkoutLayout = () => {
  const goods = getDataStorage('goods');
  const carts = checkoutCarts(goods);
  const checkoutLayout = createTag("div", "basket__checkout--wrapper");
  const subtotal = goods.reduce((acc, item) => acc + item.price, 0);
  checkoutLayout.innerHTML = `
    <div class="basket__checkout--box">
        <p class="basket__checkout--title">
            <input type="radio" class="basket__checkout--radio" disabled  id="check-address"/>
            Shipping Address
        </p>
        <p class="basket__checkout--subtitle">email</p>
        <input type="email" class="basket__checkout--email order__field"/>
        <p class="basket__checkout--subtitle">Full name</p>
        <input type="text" class="basket__checkout--name order__field"/>
        <p class="basket__checkout--subtitle">Address</p>
        <textarea
            class="basket__checkout--textarea order__field"
            cols="30"
            rows="10"
        ></textarea>
        <p class="basket__checkout--subtitle">State/Province</p>
        <input type="text" class="basket__checkout--state order__field"/>
        <p class="basket__checkout--subtitle">City</p>
        <input type="text" class="basket__checkout--city order__field"/>
        <p class="basket__checkout--subtitle">Phone number</p>
        <input type="text" class="basket__checkout--phone order__field"/>
        <div class="basket__checkout--ask">
            <input type="checkbox" class="basket__checkout--checkbox" />
            <span>Set default shipping address</span>
        </div>
        </div>
        <div class="basket__checkout--box">
            <p class="basket__checkout--title">
                <input type="radio" class="basket__checkout--radio" id="delivery-method" disabled/>
                Delivery method
            </p>
        <div class="basket__checkout--delivery">
        <div class="basket__checkout--price">
            <input
                type="checkbox"
                class="basket__checkout--checkbox"
                id="delivery-checkbox"
            /><label for="delivery-checkbox">₦ 2,000</label>
            <p>Delivery fee</p>
            <p>Door delivery</p>
        </div>
        <div class="basket__checkout--method">
            <p class="basket__checkout--title">
                <input type="radio" class="basket__checkout--radio" id="payment-methods" disabled/>
                Payment Methods
            </p>
            <div class="basket__checkout--card">
            <input type="checkbox" class="basket__checkout--checkbox payment__method" id="pay-сard"/>
            <div class="basket__checkout--description">
                <p class="basket__checkout--pay">Pay with card</p>
                <p class="basket__checkout--text">
                    (Get 5% off total price and money back guarantee)
                </p>
            </div>
        </div>
            <p class="basket__checkout--warning">
                You will be redirected to Paystack payment gateway
            </p>
            <div class="basket__checkout--delivery">
                <input
                  type="checkbox"
                  class="basket__checkout--checkbox payment__method"
                  data-price="1"
                />
                <label class="basket__checkout--pay"
                  >Pay on delivery</label
                >
                <ul class="basket__checkout--list">
                    <li class="basket__checkout--text">
                        Kindly note that we will only accept POS payment option on
                        delivery
                    </li>
                    <li class="basket__checkout--text">
                        You have to make payment before opening package
                    </li>
                    <li class="basket__checkout--text">
                        Once the seal is broken, item can only be returned if
                        damaged or defective
                    </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="basket__checkout--box">
          <p class="basket__checkout--summary">ORDER SUMMARY</p>
          <div class="basket__checkout--goods">
            ${carts}
          </div>
          <div class="basket__checkout--score">
            <p class="basket__checkout--subtotal">
              <span>Cart sub-total</span><span>₦ ${subtotal}</span>
            </p>
            <p class="basket__checkout--total">
              <span>TOTAL</span><span>₦ ${subtotal}</span>
            </p>
          </div>
          <div class="basket__checkout--conditions">
            <p>Is this a gift?</p>
            <div class="basket__checkout--btns">
              <button class="basket__checkout--yes">Yes</button>
              <button class="basket__checkout--no active">No</button>
            </div>
          </div>
          <p class="basket__checkout--agreement">
              A complimentary gift receipt will be included in the package, and 
              prices will be hidden on the receipt.
            </p>
          <button class="basket__checkout--order">PLACE ORDER</button>
        </div>
`;
  return checkoutLayout;
}