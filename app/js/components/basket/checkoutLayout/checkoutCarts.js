export const checkoutCarts = (goods) => {
    const carts = goods.map(({ img, title, size, price }) => {
        return `<div class="basket__checkout--cart">
        <img src="${img}" alt="img" />
        <div class="basket__checkout--options">
          <p class="basket__checkout--name">${title}</p>
          <p class="basket__checkout--size">Size - ${size}</p>
          <p class="basket__checkout--price">₦ ${price}</p>
          <div class="basket__checkout--counter">
            <button class="basket__checkout--plus">+</button>
            <p class="basket__checkout--count">1</p>
            <button class="basket__checkout--minus">-</button>
          </div>
        </div>
      </div>`
    }).join('')
  return carts;
}