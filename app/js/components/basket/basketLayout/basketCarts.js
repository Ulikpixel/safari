export const basketCarts = (goods) => {
  const carts = goods.map(({ img, title, size, price, id }) => {
    return `
    <div class="basket__product">
    <div class="basket__product--description">
      <div class="basket__product--info">
        <img src="${ img }" alt="logo" />
        <div class="basket__product--details">
          <p class="basket__product--subtitle">${ title }</p>
          <div class="basket__product--size">
            Size - <span>${ size }</span>
          </div>
        </div>
      </div>
      <div class="basket__product--btns">
        <button class="basket__product--favorites">
          <svg width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" >
            <path d="M8 2.782l-.405-.56c-.274-.38-.582-.7-.917-.959h0A3.525 3.525 0 004.47.5c-1.098 0-2.1.438-2.833 1.232L8 2.782zm0 0l.405-.56M8 2.782l.405-.56m0 0c.275-.38.582-.7.917-.959A3.525 3.525 0 0111.53.5c1.098 0 2.1.438 2.833 1.232.726.787 1.137 1.875 1.137 3.076 0 1.21-.444 2.34-1.472 3.58h0c-.935 1.128-2.288 2.283-3.911 3.666h0l-.015.012c-.547.467-1.168.996-1.813 1.56h0a.438.438 0 01-.578 0h0a187.98 187.98 0 00-1.825-1.57l-.002-.001h0m2.521-9.833l-2.521 9.833m0 0C4.26 10.67 2.907 9.515 1.972 8.388.944 7.148.5 6.018.5 4.808c0-1.2.411-2.289 1.137-3.076l4.247 10.323z" stroke="#ED165F" />
          </svg>
          MOVE TO FAVORITES
        </button>
        <button class="basket__product--remove" data-id='${ id }'>
          <svg
            width="16" height="16" fill="none" xmlns="http://www.w3.org/2000/svg" >
            <path d="M7.999 0A8 8 0 000 8a8 8 0 107.999-8zm3.44 10.166l-1.275 1.273S8.148 9.276 7.998 9.276c-.148 0-2.164 2.163-2.164 2.163l-1.275-1.273S6.724 8.18 6.724 8.003c0-.18-2.165-2.167-2.165-2.167l1.275-1.275s2.032 2.164 2.164 2.164c.133 0 2.166-2.164 2.166-2.164l1.274 1.275S9.273 7.852 9.273 8.003c0 .144 2.165 2.163 2.165 2.163z" fill="#ED165F" />
          </svg>
          REMOVE
        </button>
      </div>
    </div>
    <div class="basket__product--prices">
    <div class="basket__product--text">price</div>
      <div class="basket__product--totalPrice">₦<span>${ price }</span></div>
    </div>
  </div>  
    `
  })
  return carts
}


