import { createTag } from "../../../utils/createUtils/createTag.js";
import { NOGOODS } from '../../../constains/noGoods.js';
import { getDataStorage } from '../../../localStorage/localStorage.js';
import { basketCarts } from './basketCarts.js';

export const basketLayout = () => {
  const goods = getDataStorage('goods');
  const basketGoods = createTag("div", "container");
  const total = goods.reduce((acc, item) => acc + item.price, 0);
  basketGoods.innerHTML = `  
  <h4 class="basket__title">
    Shopping Cart (<span id="basket-counter">${goods.length}</span> item)
  </h4>
  <div class="basket__wrapper">
    ${goods.length ? basketCarts(goods) : '<h1 class="warning">There are no items in your cart!</h1>'}
  </div>
  <div class="basket__total">
    <p class="basket__total--result">TOTAL: ₦<span>${total}</span></p>
    <p class="basket__total--text">Delivery fee not included yet</p>
  </div>
  <div class="basket__choice">
    <button class="basket__choice--continue">CONTINUE SHOPPING</button>
    <button class="basket__choice--checkout">PROCEED TO CHECKOUT</button>
  </div>
`;
  return basketGoods;
};

