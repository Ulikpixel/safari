import { auth } from '../../data/firebase.js';
import { openModal } from '../../utils/sendUitls/openModal.js';

export const openPageFavorites = (basketPage) => {
    basketPage.addEventListener('click', (e) => {
        const item = e.target;
        if (item.className.includes('basket__product--favorites')) {
            auth.onAuthStateChanged(user => {
                if (user) {
                    // open page account
                    const accounLink = document.querySelectorAll("[data-link='4']");
                    const sidebarItems = document.querySelectorAll('.account__sidebar--item');
                    accounLink.forEach(el => el.click());
                    sidebarItems[3].click();
                } else {
                    openModal()
                }
            });
        };
    });
};




