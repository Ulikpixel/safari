import { removeGoodsStorage } from '../../localStorage/localStorage.js';

export const removeGoods = (basketRender) => {
    const wrapper = document.querySelector('.basket__wrapper');
    wrapper.addEventListener('click', (e) => {
        const item = e.target;
        if (item.className.includes('basket__product--remove')) {
            const id = +item.dataset.id;
            removeGoodsStorage('goods', id);
            basketRender();
        };
    });
};

