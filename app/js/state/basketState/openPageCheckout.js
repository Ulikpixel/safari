import { getDataStorage } from '../../localStorage/localStorage.js';
import { auth } from '../../data/firebase.js';
import { openModal } from '../../utils/sendUitls/openModal.js';
import { ordering } from './ordering.js';
import { orderGoods } from './orderGoods.js';

export const openPageCheckout = (basketPage, checkoutLayout) => {
    const checkoutBtn = document.querySelector('.basket__choice--checkout');
    checkoutBtn.addEventListener('click', () => {
        const goods = getDataStorage('goods');
        auth.onAuthStateChanged(user => {
            if (user) {
                if (goods.length) {
                    while (basketPage.firstChild) {
                        basketPage.removeChild(basketPage.firstChild);
                    }
                    basketPage.append(checkoutLayout());
                    ordering();
                    orderGoods();
                } else {
                    alert('there are no items in the cart!!')
                }
            } else {
                openModal();
            }
        });
    });
};