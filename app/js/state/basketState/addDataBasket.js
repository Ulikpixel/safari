import { setGoodsStorage } from "../../localStorage/localStorage.js";
import { database } from '../../data/firebase.js';

export const addDataBasket = (renderBasket) => {
    const goodsWrapper = document.querySelector("#goods-wrapper");
    const shoesWrapper = document.querySelector("#shoes-wrapper");
    const clothesWrapper = document.querySelector("#clothes-wrapper");
    const accessoriesWrapper = document.querySelector("#accessories-wrapper");
    const wrapperArray = [goodsWrapper, shoesWrapper, clothesWrapper, accessoriesWrapper];
    database.ref("/goods/").on("value", (snpashot) => {
        const goods = snpashot.val();
        wrapperArray.forEach(item => {
            item.addEventListener('click', (e) => {
                const item = e.target;
                if (item.className.includes("goods__basket--add")) {
                    const id = +item.dataset.id;
                    const result = goods.filter((data) => data.id === id);
                    setGoodsStorage('goods', result);
                    renderBasket();
                };
            });
        });
    })
};

