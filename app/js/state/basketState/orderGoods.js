import { getDataStorage } from '../../localStorage/localStorage.js';
import { validPrices } from '../../utils/validUtils/validPrices.js';
import { renderPageBasket } from '../../components/basket/basketPages.js';
export const orderGoods = () => {
    const btnOrder = document.querySelector('.basket__checkout--order');
    const modalWindow = document.querySelector('.modal__order');
    const addressCheckbox = document.querySelector('#check-address');
    const paymentCheck = document.querySelector('#payment-methods');
    const popup = document.querySelector('.modal__order--popup');
    const deliveryCheckbox = document.querySelector('#delivery-checkbox');
    const paymentCheckbox = document.querySelector('#pay-сard');
    const counter = document.querySelectorAll('.basket__checkout--count');
    const score = document.querySelector('.modal__order--score');
    const totalPrices = document.createElement('div');
    totalPrices.className = 'modal__order--result';

    btnOrder.addEventListener('click', () => {
        if (addressCheckbox.checked && paymentCheck.checked === false) {
            alert('Fill in all the fields!')
        } else if (addressCheckbox.checked === false) {
            alert('you must provide an address!')
        } else if (paymentCheck.checked === false) {
            alert('Specify the method!')
        } else {
            modalWindow.classList.add('active');
            popup.classList.add('active');
            const goods = getDataStorage('goods');
            const subtotal = goods.reduce((acc, item, i) => acc + item.price * +counter[i].innerText, 0);
            const total = validPrices(subtotal);
            // modal Layout     
            while (score.firstChild) {
                score.removeChild(score.firstChild);
            }
            totalPrices.innerHTML = `
                <h2 class="modal__order--title">You have successfully completed your order!</h2>
                <p class="modal__order--subtotal">Cart sub-total ${subtotal}</p>
                ${deliveryCheckbox.checked ? `
                    <p class="modal__order--delivery">Delivery fee ₦ 2,000</p>
                ` : ''}
                ${paymentCheckbox.checked ? `
                    <p class="modal__order--discount">Card discount -5%</p>
                ` : ''}
                <p class="modal__order--total">TOTAL - <span>${total}₦</span></p> 
            `
            score.append(totalPrices);
            renderPageBasket();
        };
    });
};