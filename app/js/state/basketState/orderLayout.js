import { getDataStorage } from '../../localStorage/localStorage.js';
import { validPrices } from '../../utils/validUtils/validPrices.js';
const prices = document.createElement('div');
prices.className = 'basket__price';

export const orderLayout = () => {
    const score = document.querySelector('.basket__checkout--score');
    const deliveryCheckbox = document.querySelector('#delivery-checkbox');
    const paymentCheckbox = document.querySelector('#pay-сard');
    const counter = document.querySelectorAll('.basket__checkout--count');
    const goods = getDataStorage('goods');
    const subtotal = goods.reduce((acc, item, i) => acc + item.price * +counter[i].innerText, 0);
    const total = validPrices(subtotal);
    prices.innerHTML = `
        <div>
            <p class="basket__checkout--subtotal">
                <span>Cart sub-total</span><span>₦ ${subtotal}</span>
            </p>
            ${paymentCheckbox.checked ? `<p class="basket__checkout--discount">
                <span>Card discount</span><span>-5%</span>
            </p>` : ''}
            ${deliveryCheckbox.checked ? `<p class="basket__checkout--delivery">
                <span>Card discount</span><span>₦ 2,000</span>
            </p>` : ''}
            <p class="basket__checkout--total">
                <span>TOTAL</span><span>₦ ${total}</span>
            </p>
        </div>
    `
    while (score.firstChild) {
        score.removeChild(score.firstChild);
    }
    score.append(prices);
};