import { orderLayout } from './orderLayout.js';

export const ordering = () => {
    const deliveryCheck = document.querySelector('#delivery-method');
    const deliveryCheckbox = document.querySelector('#delivery-checkbox');
    const paymentCheck = document.querySelector('#payment-methods');
    const paymentCheckbox = document.querySelectorAll('.payment__method');
    const fieldOrder = document.querySelectorAll('.order__field');
    const addressCheckbox = document.querySelector('#check-address');
    const conditionBtns = document.querySelectorAll('.basket__checkout--btns button');
    const orderAgremeent = document.querySelector('.basket__checkout--agreement');
    const plusBtns = document.querySelectorAll('.basket__checkout--plus');
    const minusBtns = document.querySelectorAll('.basket__checkout--minus');
    const counter = document.querySelectorAll('.basket__checkout--count');
    deliveryCheckbox.addEventListener('change', () => {
        if (deliveryCheckbox.checked === true) {
            deliveryCheck.checked = true;
        } else {
            deliveryCheck.checked = false;
        };
        orderLayout();
    });
    paymentCheckbox.forEach(item => {
        item.addEventListener('change', () => {
            paymentCheckbox.forEach(el => el.checked = false);
            item.checked = true;
            paymentCheck.checked = true;
            orderLayout();
        });
    });
    fieldOrder.forEach(item => {
        item.addEventListener('input', () => {
            if (fieldOrder[0].value &&
                fieldOrder[1].value &&
                fieldOrder[2].value &&
                fieldOrder[3].value &&
                fieldOrder[4].value &&
                fieldOrder[5].value !== '') {
                addressCheckbox.checked = true;
            } else {
                addressCheckbox.checked = false;
            }
        });
    });
    conditionBtns.forEach(item => {
        item.addEventListener('click', () => {
            conditionBtns.forEach(el => el.classList.remove('active'));
            if (item.innerText === 'Yes') {
                item.classList.add('active');
                orderAgremeent.classList.add('active');
            } else {
                item.classList.add('active');
                orderAgremeent.classList.remove('active');
            }
        });
    });
    plusBtns.forEach((item, index) => {
        item.addEventListener('click', () => {
            +counter[index].innerText++;
            orderLayout();
        });
    });
    minusBtns.forEach((item, index) => {
        item.addEventListener('click', () => {
            const count = +counter[index].innerText;
            if(count === 0){
                counter[index].innerText = 0;
            } else {
                +counter[index].innerText--;
            }
            orderLayout();
        });
    });
};

