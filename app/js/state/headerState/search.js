import { database, auth } from '../../data/firebase.js';
import { goodsFilter } from '../../utils/arrayUtils/goodsFilter.js';
import { getCart } from './getCart.js';
import { setGoodsStorage } from '../../localStorage/localStorage.js';
import { renderPageBasket } from '../../components/basket/basketPages.js';
import { openModal } from '../../utils/sendUitls/openModal.js';
const search = document.querySelector('.header__search');
const fieldSearch = document.querySelector('.header__search input');
const btnSearch = document.querySelector('.header__search button');
const cartModal = document.querySelector('.cart__modal');
const btnClose = document.querySelector('.cart__modal--close');
const cartPopup = document.querySelector('.cart__modal--popup');
const goodsRef = database.ref("/goods/");
const productList = document.createElement('ol');
productList.className = 'header__products';
const optionsCart = document.createElement('div');
optionsCart.className = 'cart__modal--wrapper';

// search product
btnSearch.addEventListener('click', () => {
    const val = fieldSearch.value.toLowerCase().trim();
    goodsRef.on("value", (snpashot) => {
        const goods = snpashot.val();
        const nameGoods = goods.map(product => product.title);
        const result = nameGoods.filter(name => name.toLowerCase().includes(val));
        productList.innerHTML = result.map(name => `<li class="header__names">${name}</li>`).join('');
        search.append(productList);
    });
});

fieldSearch.addEventListener('input', (e) => {
    if (e.target.value.trim() === '') {
        search.removeChild(productList);
    };
});

fieldSearch.addEventListener('keyup', (e) => {
    if (e.keyCode == 13) {
        if (fieldSearch.value.trim() !== '') {
            btnSearch.click();
        };
    };
});

// send modal with options product
productList.addEventListener('click', (e) => {
    const item = e.target;
    if (item.className.includes('header__names')) {
        goodsRef.on("value", (snpashot) => {
            const goods = snpashot.val();
            const result = goodsFilter(goods)('title', item.innerText);
            const cart = getCart(result);
            cartModal.classList.add('active');
            cartPopup.classList.add('active');
            optionsCart.innerHTML = cart;
            cartModal.append(optionsCart);
            while (fieldSearch.firstChild) {
                fieldSearch.removeChild(fieldSearch.firstChild);
            }
        });
    };
});

const closeModal = () => {
    cartModal.classList.remove('active');
    cartPopup.classList.remove('active');
}

cartPopup.addEventListener('click', closeModal);
btnClose.addEventListener('click', closeModal);

optionsCart.addEventListener('click', (e) => {
    const item = e.target;
    goodsRef.on("value", (snpashot) => {
        const goods = snpashot.val();
        if (item.className.includes('cart__modal--basket')) {
            const id = +item.dataset.id;
            fieldSearch.value = '';
            search.removeChild(productList);
            const product = goods.filter(item => item.id === id);
            setGoodsStorage('goods', product);
            closeModal();
            renderPageBasket();
        } else if (item.className.includes('cart__modal--favorites')) {
            fieldSearch.value = '';
            search.removeChild(productList);
            auth.onAuthStateChanged(user => {
                if (user) {
                    const id = +item.dataset.id;
                    const product = goods.filter(item => item.id === id);
                    setGoodsStorage('favorites', product);
                    closeModal();
                    renderPageBasket();
                } else {
                    closeModal();
                    openModal();
                };
            });
        };
    });
});