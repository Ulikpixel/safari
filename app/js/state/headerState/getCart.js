export const getCart = (product) => {
    const cart = product.map(({ id, img, title, size, price }) => {
        return `
            <img src="${img}" alt="logo">
            <div class="cart__modal--description">
                <p class="cart__modal--title">${title}</p>
                <p class="cart__modal--size">size - ${size}</p>
                <div class="cart__modal--price">price - ${price}</div>
            </div>
            <button class="cart__modal--basket" data-id="${id}">add basket</button>
            <button class="cart__modal--favorites" data-id="${id}">add favorites</button>
        `
    })
    return cart;
}