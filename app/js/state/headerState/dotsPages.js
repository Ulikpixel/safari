const headerLink = document.querySelectorAll(".link__pages");
const pagesContent = document.querySelectorAll(".main");


headerLink.forEach((item) => {
  item.addEventListener("click", (e) => {
    e.preventDefault();
    const index = +item.dataset.link;
    pagesContent.forEach((el) => el.classList.remove("open__pages"));
    headerLink.forEach((el) => el.classList.remove('link_active'));
    item.classList.add('link_active');
    pagesContent[index].classList.add("open__pages");
  });
});


