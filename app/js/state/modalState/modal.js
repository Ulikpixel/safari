import { auth } from '../../data/firebase.js';
import { closeModal } from '../../utils/sendUitls/closeModal.js';
const modalPopup = document.querySelector('.modal__popup');
const closeBtn = document.querySelector('.modal__sign--close');
const modalForm = document.querySelector('.modal__sign--form');
const createBtn = document.querySelector('.modal__sign--create');
const accountLink = document.querySelectorAll("[data-link='4']");
const headerLink = document.querySelectorAll(".link__pages");
const pagesContent = document.querySelectorAll(".main");

modalPopup.addEventListener('click', closeModal);
closeBtn.addEventListener('click', closeModal);

modalForm.addEventListener('submit', e => {
    e.preventDefault();
    const item = e.target;
    const email = item['email'].value.trim();
    const password = item['password'].value.trim();
    if (email.length && password.length >= 5) {
        auth.signInWithEmailAndPassword(email, password)
            .then((cred) => {
                item['email'].value = '';
                item['password'].value = '';
                closeModal();
            })
            .catch((err) => alert('Please enter your email correctly'));
    } else {
        alert("Fill in the field!");
    };
});

createBtn.addEventListener('click', () => {
    closeModal();
    setTimeout(() => {
        pagesContent.forEach((el) => el.classList.remove("open__pages"));
        headerLink.forEach((el) => el.classList.remove('link_active'));
        pagesContent[4].classList.add('open__pages');
        accountLink.forEach(el => el.classList.add('link_active'));
    }, 600);
});



