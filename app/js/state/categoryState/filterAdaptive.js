import { media } from "../../utils/validUtils/media.js";

const filterBtn = document.querySelectorAll(".category__subtitle");
filterBtn.forEach((item) => {
  item.addEventListener("click", () => {
    if (media("(max-width: 992px)")) {
      item.classList.toggle("category__subtitle_active");
      const element = item.nextElementSibling;
      const content = element.nextElementSibling;
      content.classList.toggle("open__filter");
    }
  });
});
