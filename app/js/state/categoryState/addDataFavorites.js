import { setGoodsStorage } from '../../localStorage/localStorage.js';
import { auth } from '../../data/firebase.js';
import { openModal } from '../../utils/sendUitls/openModal.js';

export const addDataFavorites = (goods) => {
    const favoritesBtns = document.querySelectorAll('.goods__basket--like');
    favoritesBtns.forEach(btn => {
        btn.addEventListener('click', () => {
            auth.onAuthStateChanged(user => {
                if(user){
                    const id = +btn.dataset.id;
                    const result = goods.filter(data => data.id === id);
                    setGoodsStorage('favorites', result);
                } else {
                    openModal();
                }
            });
        });
    });
};
