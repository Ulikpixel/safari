import { database } from '../../data/firebase.js';
const adminForm = document.querySelector('#admin-form');
const fieldPrice = document.querySelector('#price-product');
const goodsRef = database.ref('/goods/');

adminForm.addEventListener('submit', (e) => {
    e.preventDefault();
    const getValue = (name) => e.target[name].value.trim();
    const title = getValue('name');
    const img = getValue('img');
    const category = getValue('category');
    const type = getValue('type');
    const size = getValue('size');
    const color = getValue('color');
    const price = +getValue('price');
    if (title === '' || img === '' || size === '' || category === '') {
        alert('Fill in all the fields');
    } else if (color === 'colors') {
        alert('indicate color')
    } else if (type === 'types') {
        alert('select product type');
    } else if (price === 0) {
        alert('indicate the price of your product');
    } else {
        const id = Date.now();
        const product = {
            id, title,
            img, type,
            price, size,
            color, category,
        };
        goodsRef.child(id).push(product);
        e.target.name.value = '';
        e.target.img.value = '';
        e.target.size.value = '';
        e.target.price.value = '';
        e.target.category.value = '';
    }
});

fieldPrice.addEventListener('input', (e) => {
    if (isNaN(e.target.value)) {
        e.target.value = e.target.value.replace(/[^\d]/g, '');
    }
});


