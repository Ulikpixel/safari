import { auth } from '../../data/firebase.js';

export const logout = () => {
    const logout = document.querySelector('.account__sidebar--out');
    logout.addEventListener('click', () => {
        auth.signOut();
    })
}