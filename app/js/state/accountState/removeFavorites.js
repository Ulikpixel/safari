import { getDataStorage, removeGoodsStorage } from '../../localStorage/localStorage.js';
import { NOGOODS } from '../../constains/noGoods.js';
import { favoritesCarts } from '../../components/account/user/favoritesCarts.js';

export const removeFavorites = () => {
    const wrapper = document.querySelector('.account__favorites--wrapper');
    wrapper.addEventListener('click', (e) => {
        const item = e.target;
        if (item.className.includes('account__favorites--remove')) {
            const id = +item.dataset.id;
            removeGoodsStorage('favorites', id);
            const favoritesGoods = getDataStorage('favorites');
            const carts = favoritesCarts(favoritesGoods);
            while(wrapper.firstChild) {
                wrapper.removeChild(wrapper.firstChild);
              };
            favoritesGoods.length ? wrapper.append(...carts) : wrapper.append(NOGOODS);
        };
    });
};

