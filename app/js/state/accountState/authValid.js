import { auth, database } from "../../data/firebase.js";

export const authValid = () => {
  const signForm = document.querySelector('.account__sign--form');
  const createForm = document.querySelector('.account__create--form');
  signForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const item = e.target;
    const email = item["email"].value.trim();
    const password = item["password"].value.trim();
    const checkbox = item.checkbox;
    if (email.length && password.length >= 5) {
      if (checkbox.checked === true) {
        auth.signInWithEmailAndPassword(email, password)
          .then((cred) => {
            item['email'].value = '';
            item['password'].value = '';
            item['checkbox'].checked = false;
          })
          .catch((err) => alert('Please enter your email correctly'));
      } else {
        alert("you did not agree with the condition");
      }
    } else {
      alert("Fill in the field!");
    }
  });

  createForm.addEventListener("submit", (e) => {
    e.preventDefault();
    const item = e.target;
    const name = item["name"].value.trim();
    const lastName = item["lastname"].value.trim();
    const email = item["email"].value.trim();
    const password = item["create_password"].value.trim();
    const confirmPassword = item["confirm_password"].value.trim();
    const checkbox = item.checkbox;
    if (
      name.length &&
      lastName.length &&
      email.length &&
      password.length &&
      confirmPassword.length >= 5
    ) {
      if (checkbox.checked === true) {
        if (password === confirmPassword) {
          auth.createUserWithEmailAndPassword(email, password)
            .then((cred) => {
              const id = auth.currentUser.uid;
              database.ref('users/' + id).set({ 
                name: name, 
                lastname: lastName, 
                email: email,
                role: 'user',
              })
              item['name'].value = '';
              item['lastname'].value = '';
              item['email'].value = '';
              item['create_password'].value = '';
              item['confirm_password'].value = '';
              item['checkbox'].checked = false;
            })
            .catch((err) => alert('This email already exists'));
        } else {
          alert("The confirm password was entered incorrectly");
        }
      } else {
        alert("you did not agree with the condition");
      }
    } else {
      alert("Fill in the field!");
    }
  });
}

