export const sidebar = () => {
    const sidebarBtn = document.querySelector('.account__sidebar--title');
    const sidebar = document.querySelector('.account__sidebar');
    const sidebarItems = document.querySelectorAll('.account__sidebar--item');
    const accountBoxes = document.querySelectorAll('.account__boxes');
    // open sidebar
    sidebarBtn.addEventListener('click', () => {
        sidebar.classList.toggle('active');
    });
    // tabs sidebar
    sidebarItems.forEach((item, i) => {
        item.addEventListener('click', () => {
            sidebarItems.forEach(el => el.classList.remove('active'))
            accountBoxes.forEach(el => el.classList.remove('active'))
            item.classList.add('active')
            accountBoxes[i].classList.add('active')
            sidebar.classList.remove('active');
        });
    });
};