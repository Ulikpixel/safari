import { getDataStorage } from '../../localStorage/localStorage.js';
import { favoritesCarts } from '../../components/account/user/favoritesCarts.js';
import { NOGOODS } from '../../constains/noGoods.js';

export const getFavorites = () => {
    const favoritesBtn = document.querySelector('#favorites-btn');
    const wrapper = document.querySelector('.account__favorites--wrapper');
    favoritesBtn.addEventListener('click', () => {
        const favoritesGoods = getDataStorage('favorites');
        const carts = favoritesCarts(favoritesGoods);
        while (wrapper.firstChild) {
            wrapper.removeChild(wrapper.firstChild);
        };
        favoritesGoods.length ? wrapper.append(...carts) : wrapper.append(NOGOODS);
    });
};