import { database } from '../../data/firebase.js';

export const changesAccount = (id, role) => {
    const userInfo = document.querySelector('.account__information--information');
    const changesBtn = document.querySelector('.account__information--btn'); 
    changesBtn.addEventListener('click', () => {
        const name = userInfo.name.value.trim();
        const lastname = userInfo.lastname.value.trim();
        const email = userInfo.email.value.trim();
        const gender = userInfo.gender.value;
        const data = userInfo.data.value.trim();
        if(name.length && lastname.length && email.length && data.length >= 5){
            database.ref(`users/${id}/`).set({
                name: name,
                lastname: lastname,
                email: email,
                gender: gender,
                data: data,
                role: role,
            })
        } else {
            alert('Fill in all the fields!')
        }
    })
}