import { goodsFilter } from "./goodsFilter.js";
import { numValid } from "../validUtils/numValid.js";

export const filterSize = (arr, el) => {
  el.classList.add("active");
  const item = numValid(el.innerText);
  if (typeof item === "string") {
    const result = arr.filter(data => data.size === item.toLowerCase());
    return result;
  } else {
    const result = arr.filter((data) => data.size === item);
    return result;
  }
};
