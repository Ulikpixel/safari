import { goodsFilter } from "./goodsFilter.js";

export const filterCategory = (arr) => {
  return (el, type) => {
    el.classList.add('active');
    if(el.innerText === 'All'){
        return arr;
    } else {
        const result = goodsFilter(arr)(type, el.innerText)
        return result;
    }
  }
};
