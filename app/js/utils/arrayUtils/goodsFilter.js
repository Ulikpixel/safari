export const goodsFilter = (arr) => {
  return (key, name) => {
    const result = arr.filter((item) =>
      item[key].toLowerCase().trim().includes(name.toLowerCase().trim())
    );
    return result;
  };
};
