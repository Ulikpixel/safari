import { WARNING } from "../../constains/warning.js";
import { componentsRender } from "../renderUtils/componentsRender.js";
import { goodsRender } from "../renderUtils/renderGood.js";

export const validGoods = (arr) => {
  return (tagGoods, tagDots) => {
    if (arr.length === 0) {
        componentsRender([tagGoods, tagDots])([WARNING, '']);
    } else {
        const [goodsLayout, dotsLayout] = goodsRender(arr)(1, 6);
        componentsRender([tagGoods, tagDots])([goodsLayout, dotsLayout]);
    }
  };
};
