const modalWindow = document.querySelector('.modal__sign');
const modalPopup = document.querySelector('.modal__popup');

export const openModal = () => {
    modalWindow.classList.add('active');
    modalPopup.classList.add('active');
}