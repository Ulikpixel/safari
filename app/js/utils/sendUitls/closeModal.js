const modalWindow = document.querySelector('.modal__sign');
const modalPopup = document.querySelector('.modal__popup');

export const closeModal = () => {
    modalWindow.classList.remove('active');
    modalPopup.classList.remove('active');
};