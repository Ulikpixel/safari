export const componentsRender = (tag) => {
  return (data) => {
    tag.forEach((item) => {
      while(item.firstChild) {
        item.removeChild(item.firstChild);
      }
    });
    tag.forEach((item, index) => item.append(data[index]));
  };
};



