import { createTag } from "../createUtils/createTag.js";

export const renderJSX = (tagName, className) => {
    const tag = createTag(tagName, className);
    return (jsx) => {
        tag.innerHTML = jsx;
        return tag;
    }
}