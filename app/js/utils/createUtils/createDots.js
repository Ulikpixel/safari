import { createTag } from "../createUtils/createTag.js";

export const createDots = (arr, showPage) => {
  // showPage - number that will show a certain number of products
  const dotsList = createTag("ul", "pagination__list");
  const dotsData = Math.ceil(arr.length / showPage); // divide by the length of the array
  for (let i = 1; i <= dotsData; i++) {
    const li = document.createElement("li");
    li.className = "pagination__dot";
    li.innerHTML = i;
    dotsList.append(li);
  }
  const dots = dotsList.querySelector(".pagination__dot");
  dots.classList.add("pagination__dot_active");
  return dotsList;
};
