export const createTag = (tagName, className) => {
    const tag = document.createElement(tagName);
    tag.className = className;
    return tag;
}
