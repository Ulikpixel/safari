import { goodsRender } from "../renderUtils/renderGood.js";

export const createGoods = (arr) => {
  return (tagGoods, tagDots) => {
    const [goodsLayout, dotsLayout] = goodsRender(arr)(1, 6);
    while(tagGoods.firstChild) {
      tagGoods.removeChild(tagGoods.firstChild);
    }
    while(tagDots.firstChild) {
      tagDots.removeChild(tagDots.firstChild);
    }
    tagGoods.append(goodsLayout);
    tagDots.append(dotsLayout);
  };
};
